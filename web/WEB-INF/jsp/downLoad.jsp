<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2022/6/11
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>网络U盘</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="screen" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="logo">
            <div style="color: #888; font-size: 36px; font-family: 微软雅黑">
                网络U盘
            </div>
            <div style="color: #333; font-size: 12px; font-family: 微软雅黑">
                永久的网络数据存储专家
            </div>
        </div>
        <div id="nav-box">
            <div id="toll">
                咨询电话：66666666 / 88888888
            </div>
            <div id="nav">
                <div id="nav-bg">
                    <ul>
                        <li>
                            <a href="/file/toDownLoad">下载</a>
                        </li>
                        <li>
                            <a href="/user/toregister">注册</a>
                        </li>
                        <li>
                            <a href="contact.html">帮助</a>
                        </li>
                    </ul>
                </div>
                <div id="left-nav"></div>
            </div>
        </div>
    </div>
    <div id="content">
        <h4 class="service-head">请输入文件提取码</h4>
        <form action="${pageContext.request.contextPath}/file/download" method="post">
            <P>提取码：<input type="text" name="fileCode" size="40"/>
            <p><input type="submit" value="下载"/>
        </form>
    </div>
</div>
<div id="footer">
    项目设计 : XXXX
</div>
</body>
</html>
