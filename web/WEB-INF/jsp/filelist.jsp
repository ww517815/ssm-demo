<%--
  Created by IntelliJ IDEA.
  User: WiK-
  Date: 2022/6/10
  Time: 22:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>网络U盘</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="screen" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
    <style>
        td{
            padding:2px;
            font-family:宋体;
        }
    </style>
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="logo">
            <div style="color: #888; font-size: 36px; font-family: 微软雅黑">
                网络U盘
            </div>
            <div style="color: #333; font-size: 12px; font-family: 微软雅黑">
                永久的网络数据存储专家
            </div>
        </div>
        <div id="nav-box">
            <div id="toll">
                咨询电话：66666666 / 88888888
            </div>
            <div id="nav">
                <div id="nav-bg">
                    <ul>
                        <li>
                            <a href="/file/toDownLoad">下载</a>
                        </li>
                        <li>
                            <a href="/user/toregister">注册</a>
                        </li>
                        <li>
                            <a href="contact.html">帮助</a>
                        </li>
                    </ul>
                </div>
                <div id="left-nav"></div>
            </div>
        </div>
    </div>
    <div id="content">
        <h4 class="service-head">欢迎您：${userName} </h4>
        <p>&nbsp;</p>
        <table style="border:solid 1px gray;padding:2px" width="100%">
            <thead style="background-color:#EEE">
            <tr>
                <th width="30%">文件名</th>
                <th width="20%">上传日期</th>
                <th width="15%">大小</th>
                <th width="25%">提取码</th>
                <th width="10%">操作</th>
            </tr>
            </thead>

            <tbody>
             <c:forEach var="file" items="${fileList}">
            <tr>
                <td>${file.fileName}</td>
                <td><fmt:formatDate value="${file.uploadDate}" pattern="yyyy-MM-dd"/></td>
                <td>${file.fileSize}</td>
                <td>${file.fileCode}</td>
                <td>
                 <A href="/file/download?fileCode=${file.fileCode}">下载</A>
                 <A href="/file/deleteFile?fileId=${file.fileId}">删除</A>
                 </td>
            </tr>
             </c:forEach>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <h4 class="service-head">上传文件</h4>
        <form action="/file/upload" enctype="multipart/form-data" method="post">
            <!--判断该用户是否为VIP用户-->
            <c:if test="${user.userType==2}">
                上传文件的数量：
                <select onchange="makeFile(this.options[this.selectedIndex].text)">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                </select>
            </c:if>

            <div id="divFile">
                <input type="file" name="files"/><br/>
            </div>

            <input type="submit" value="上传"/>
        </form>

        <script>
            function makeFile(v) {
                var divFile = document.getElementById('divFile');
                divFile.innerHTML='';
                for (var i=0;i<v;i++) {
                    divFile.innerHTML += '<input type="file" name="files"><br/>';
                }
            }
        </script>
    </div>
</div>
<div id="footer">
    项目设计 : XXXX
</div>
</body>
</html>

