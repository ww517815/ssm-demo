<%--
  Created by IntelliJ IDEA.
  User: WiK-
  Date: 2022/6/9
  Time: 19:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>网络U盘</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="screen" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="logo">
            <div style="color: #888; font-size: 36px; font-family: 微软雅黑">
                网络U盘
            </div>
            <div style="color: #333; font-size: 12px; font-family: 微软雅黑">
                永久的网络数据存储专家
            </div>
        </div>
        <div id="nav-box">
            <div id="toll">
                咨询电话：6666666 / 88888888
            </div>
            <div id="nav">
                <div id="nav-bg">
                    <ul>
                        <li>
                            <a href="#">下载</a>
                        </li>
                        <li>
                            <a href="about.html">注册</a>
                        </li>
                        <li>
                            <a href="contact.html">帮助</a>
                        </li>
                    </ul>
                </div>
                <div id="left-nav"></div>
            </div>
        </div>
    </div>
    <div id="content">
        <h4 class="service-head">请您注册</h4>
        <form action="/user/register" method="post">
            <P>用户名：<input name="userName" id="userName"/><span id="msg"></span>
            <P>密 码 ：<input name="password"/>
            <P>类 型 ：<input name="userType" type="radio" value="1"/> 普通会员
                      <input name="userType" type="radio" value="2"/> VIP会员
            <p><input type="submit" value="注册" id="submitBtn"/>
        </form>
    </div>
<script type="text/javascript">
$("#userName").blur(function () {
//获取输入的用户名
    var userName=$(this).val();
    $.ajax({
        url:"/user/check?userName="+userName,
        success:function (data) {
            if (data.flag){
                $("#msg").html("<font style='color: red'>"+data.message+"</font>");
                $("#submitBtn").attr("disabled",true);
            }else {
                $("#msg").html("<font style='color: green'>"+data.message+"</font>");
                $("#submitBtn").attr("disabled",false);
            }
        }
    });
});
</script>
</div>
</body>
</html>
