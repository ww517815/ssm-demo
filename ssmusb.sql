/*
Navicat MySQL Data Transfer

Source Server         : whh
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : ssmusb

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2022-06-21 21:29:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fileinfo
-- ----------------------------
DROP TABLE IF EXISTS `fileinfo`;
CREATE TABLE `fileinfo` (
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(50) DEFAULT NULL,
  `uploadDate` date DEFAULT NULL,
  `fileSize` bigint(20) DEFAULT NULL,
  `fileCode` varchar(255) DEFAULT NULL,
  `filePath` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fileId`),
  KEY `fk_userId` (`userId`),
  CONSTRAINT `fk_userId` FOREIGN KEY (`userId`) REFERENCES `userinfo` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fileinfo
-- ----------------------------
INSERT INTO `fileinfo` VALUES ('1', '12.png', '2022-06-11', '6312', '9aeb94fb25053286c973dd2a897f8fe2', 'D:/upload/whh/12.png', '14');
INSERT INTO `fileinfo` VALUES ('2', '1.jpg', '2022-06-11', '0', 'aff9c04871c9f0f270048c22ab73cad', 'D:/upload/whh/1.jpg', '14');

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `userType` int(11) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('8', '王浩', '202cb962ac59075b964b07152d234b70', '1');
INSERT INTO `userinfo` VALUES ('9', 'aaa', '202cb962ac59075b964b07152d234b70', '1');
INSERT INTO `userinfo` VALUES ('10', 'aaaa', '827ccbeea8a706c4c34a16891f84e7b', '1');
INSERT INTO `userinfo` VALUES ('12', 'bbb', '202cb962ac59075b964b07152d234b70', '0');
INSERT INTO `userinfo` VALUES ('13', 'bbbb', '202cb962ac59075b964b07152d234b70', '2');
INSERT INTO `userinfo` VALUES ('14', 'whh', '202cb962ac59075b964b07152d234b70', '2');
