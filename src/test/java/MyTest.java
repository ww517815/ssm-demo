import com.whh.pojo.UserInfo;
import com.whh.service.UserService;
import com.whh.util.Constants;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.http.HttpServletRequest;

public class MyTest {

    @Test
    public void test(HttpServletRequest request){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userServiceImpl = context.getBean("UserServiceImpl", UserService.class);
        String attribute = (String) request.getSession().getAttribute(Constants.USER_SESSION);
        UserInfo userInfo = userServiceImpl.queryByNameUser(attribute);
        System.out.println(userInfo);

    }
    }