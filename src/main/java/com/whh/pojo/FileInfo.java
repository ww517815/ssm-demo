package com.whh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileInfo {
    private int fileId;//文件Id
    private String fileName;//文件名字
    private Date uploadDate;//文件上传时间
    private long fileSize;//文件大小
    private String fileCode;//文件编码
    private String filePath;//文件路径
    private int userId; //外键

    public FileInfo(String fileName,long fileSize,String fileCode,String filePath, int userId){
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileCode = fileCode;
        this.filePath = filePath;
        this.userId = userId;
    }
}
