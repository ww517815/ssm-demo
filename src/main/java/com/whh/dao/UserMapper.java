package com.whh.dao;


import com.whh.pojo.UserInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    //添加 注册
    public int registerUser(UserInfo userInfo);
    //按照名字查询 进行校验 进行登录
    public UserInfo queryByNameUser(String userName);


}
