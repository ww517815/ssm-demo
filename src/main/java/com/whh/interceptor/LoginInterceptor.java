package com.whh.interceptor;

import com.whh.util.Constants;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
       String userName=(String)request.getSession().getAttribute(Constants.USERName_SESSION);

       if (userName!=null){
          return true;
       }
        response.sendRedirect(request.getContextPath()+"/user/toLogin");
        return false;
    }
}
