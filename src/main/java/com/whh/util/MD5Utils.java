package com.whh.util;

import org.junit.Test;

import java.security.MessageDigest;

public class MD5Utils {

    public static String createMD5(String str){
        try {
            MessageDigest messageDigest=MessageDigest.getInstance("MD5");
            byte[] oldBytes = str.getBytes("iso-8859-1");
            byte[] md5Bytes = messageDigest.digest(oldBytes);
            StringBuffer stringBuffer=new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                byte b=md5Bytes[i];
                int val=((int)b)&0xff;//加盐salt
                if (val<10){
                    stringBuffer.append(0);
                }
                stringBuffer.append(Integer.toHexString(val));
            }
            return stringBuffer.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //测试
    @Test
    public void test(){
        String str="123";
        System.out.println(MD5Utils.createMD5(str));
    }
}
