package com.whh.controller;


import com.whh.pojo.FileInfo;
import com.whh.pojo.UserInfo;
import com.whh.service.FileService;
import com.whh.service.UserService;
import com.whh.util.Constants;
import com.whh.util.DownFileUtils;
import com.whh.util.MD5Utils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/file")
public class FileInfoController {
    @Autowired
    private FileService fileService;
    //查询该用户全部文件
    @RequestMapping("/allFile")
    public String queryAllFile(Model model, HttpSession session){
        //从session中把userInfo取出来
        UserInfo userInfo = (UserInfo) session.getAttribute(Constants.USER_SESSION);
        //把会员名字存入model中用于在前端展示
        model.addAttribute("userName", userInfo.getUserName());
        //获取该会员的id用于查询他存储的所有文件
        int userId = userInfo.getUserId();
        //查询该会员的所有文件
        List<FileInfo> fileInfos = fileService.queryAllFile(userId);
        //存储在model中在前端展示
        model.addAttribute("fileList",fileInfos);
        return "filelist";
    }
    //文件上传
    @RequestMapping("/upload")
    public String upload(MultipartFile[] files,HttpSession session) throws IOException {
        //将文件上传到服务器端
        //根据用户类型针对上传做对应的限制(单文件大小的限制/剩余空间是否满足的限制)
        //从session中把userInfo取出来
        UserInfo userInfo = (UserInfo) session.getAttribute(Constants.USER_SESSION);
        System.out.println(userInfo);
        File file=new File("D:/upload/"+userInfo.getUserName());
        //统计上传文件的总大小
        File[] fileNumber=file.listFiles();//获取全部子文件
        long usedSize=0;//已用空间
        for (File f:fileNumber){
            usedSize+=f.length();
        }
        //计算剩余空间=个人空间总大小-已上传文件的总大小
        long freeSize=0;
        //普通会员
        if (userInfo.getUserType()==1){
            freeSize =32*1024*1024-usedSize;
        }else if (userInfo.getUserType()==2){
            freeSize=128*1024*1024-usedSize;
        }
        //遍历MultipartFile数组
        long uploadSize=0;
        for (MultipartFile multipartFile : files) {
            System.out.println(multipartFile);
            long fileSize = multipartFile.getSize();//获取待上传文件的大小
            if (userInfo.getUserType()==1&&fileSize>1*1024*1024){
                throw new RuntimeException("普通用户单文件大小不能超过1mb");
            }else if (userInfo.getUserType()==2&&fileSize>2*1024*1024){
                throw new RuntimeException("VIP用户单文件大小不能超过2mb");
            }
            uploadSize+=fileSize;
        }
        //判断剩余空间和待上传文件的总大小的关系
        if (uploadSize>freeSize){
            throw new RuntimeException("个人空间不足");
        }
        //执行上传
        for (MultipartFile multipartFile : files) {
            System.out.println(multipartFile);
            String fileName = multipartFile.getOriginalFilename();
            multipartFile.transferTo(new File(file,fileName));
            //讲上传的文件添加到表中
            long fileSize = multipartFile.getSize();
            String filePath="D:/upload/"+userInfo.getUserName()+"/"+fileName;
            String fileCode= MD5Utils.createMD5(filePath);//提取码
            int userId = userInfo.getUserId();
            FileInfo fileInfo=new FileInfo(fileName,fileSize,fileCode,filePath,userId);
            fileService.addFile(fileInfo);
            System.out.println(fileInfo);
        }

        return "redirect:/file/allFile";
    }

    //删除文件
    @RequestMapping("/deleteFile")
    public String deleteFile(int fileId) {
        FileInfo fileInfo = fileService.queryByIdFile(fileId);
        String filePath = fileInfo.getFilePath();
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();//删除文件
        }
        fileService.deleteFile(fileId);
        return "redirect:/file/allFile";
    }

    @RequestMapping("/toDownLoad")
    public String toDownLoad(){
        return "downLoad";
    }
    //下载文件
    @RequestMapping("/download")
    @ResponseStatus(HttpStatus.OK)
    public void download(String fileCode, HttpServletResponse response){
        System.out.println(fileCode);
        //根据提取码下载文件
        FileInfo download = fileService.download(fileCode);
        String filePath = download.getFilePath();
        File file=new File(filePath);
        DownFileUtils.downloadMyFile(file,response);
    }
}
