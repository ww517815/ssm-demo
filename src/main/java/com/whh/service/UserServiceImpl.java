package com.whh.service;

import com.whh.dao.UserMapper;
import com.whh.pojo.UserInfo;
import com.whh.util.MD5Utils;

import java.io.File;

public class UserServiceImpl implements UserService{

    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    //注册 添加用户
    @Override
    public int registerUser(UserInfo userInfo) {
        //判断用户是否为空 不是空则把密码加密存入数据库
            String md5Password = MD5Utils.createMD5(userInfo.getPassword());
            userInfo.setPassword(md5Password);

            //为每一个用户创建一个文件夹
            File file=new File("D:/upload/"+userInfo.getUserName());
            if (!file.exists()){
                //不存在则创建
                file.mkdirs();
        }

       return userMapper.registerUser(userInfo);
    }

    //根据名字查询  判断
    @Override
    public UserInfo queryByNameUser(String userName) {
        System.out.println(userName);
        return userMapper.queryByNameUser(userName);

    }
}
