package com.whh.service;

import com.whh.pojo.UserInfo;

public interface UserService {
    //注册 添加用户
    public int registerUser(UserInfo userInfo);
    //按照名字查询 进行校验
    public UserInfo queryByNameUser(String userName);
}
