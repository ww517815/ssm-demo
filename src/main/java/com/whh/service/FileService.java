package com.whh.service;

import com.whh.pojo.FileInfo;

import java.util.List;

public interface FileService {

    //查询该用户的所有文件
    public List<FileInfo> queryAllFile(int userId);
    //添加文件
    public int addFile(FileInfo fileInfo);
    //删除文件
    public int deleteFile(int fileId);
    //通过id查询数据用于删除文件
    public FileInfo queryByIdFile(int fileId);
    //下载文件
    public FileInfo download(String fileCode);
}
