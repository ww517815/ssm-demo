package com.whh.service;

import com.whh.dao.FileMapper;
import com.whh.pojo.FileInfo;

import java.util.List;

public class FileServiceImpl implements FileService{
    private FileMapper fileMapper;

    public void setFileMapper(FileMapper fileMapper) {
        this.fileMapper = fileMapper;
    }


    //查询该用户的所有文件
    @Override
    public List<FileInfo> queryAllFile(int userId) {
        System.out.println(userId);
        return fileMapper.queryAllFile(userId);
    }

    //添加文件
    @Override
    public int addFile(FileInfo fileInfo) {
        System.out.println("============================="+fileInfo);
     return fileMapper.addFile(fileInfo);
    }

    //删除
    @Override
    public int deleteFile(int fileId) {
        return fileMapper.deleteFile(fileId);
    }

    //下载
    @Override
    public FileInfo download(String fileCode) {
        return fileMapper.download(fileCode);
    }

    @Override
    public FileInfo queryByIdFile(int fileId) {
        return fileMapper.queryByIdFile(fileId);
    }
}
